#include <memory>
#include <stdint.h>
#include <cstring>

using namespace std;

template<class T>
class Vector{
private:
    T* data_;
    std::size_t capacity_;
    std::size_t size_;
    void reallocate()
    {
        if (capacity_ == 0)
        {
            delete[] data_;
            data_ = new T[1];
            capacity_ = 1;
        }
        else
        {
            capacity_ <<= 1;
            T* buf_ptr = new T[capacity_];
            memmove(buf_ptr, data_, sizeof(T) * size_);
            delete[] data_;
            data_ = buf_ptr;
        }
    }
public:
    size_t capacity() const
    {
        return capacity_;
    }
    size_t size() const
    {
        return size_;
    }
    const T* data() const
    {
        return data_;
    }
    
    T* begin() noexcept
    {
        return data_;
    }

    const T* cbegin() const noexcept
    {
        return (const T*) data_;
    }


    T* rbegin() noexcept
    {
        if (size_ == 0)
        {
            return data_;
        }
        return data_ + size_ - 1;
    }
    T* crbegin() const noexcept
    {
        return (const T*) this->rbegin();
    }
    T* end() noexcept
    {
        if (capacity_ == 0)
        {
            return 0;
        }
        if (size_ == 0)
        {
            return data_;
        }
        else
        {
            return data_ + size_;
        }
    }
    const T* cend() const noexcept
    {
        return (const T*) this->end();
    }
    T* rend() noexcept
    {
        if (capacity_ == 0)
        {
            return 0;
        }
        if (size_ == 0)
        {
            return data_;
        }
        else
        {
            return data_ - 1;
        }
    }
    const T* crend() const noexcept
    {
        return (const T*) this->rend();
    }
    void clear()
    {
        size_ = 0;
    }
    T operator[](const std::size_t pos)
    {
        return data_[pos];
    }

    T at(const std::size_t pos)
    {
        if (pos > size_ || pos < 0)
        {
            throw std::out_of_range("Specified position is out of range of vector");
        }
        return data_[pos];
    }

    void push_back(const T& val)
    {
        if (size_ == capacity_)
        {
            reallocate();
        }
        data_[size_] = move(val);
        ++size_;
    }
    void pop_back()
    {
        --size_;
    }
    void insert(const std::size_t pos, const T& val)
    {
        if (size_ == capacity_)
        {
            reallocate();
        }
        if (pos < 0 || pos >= size_)
        {
            throw std::out_of_range("Specified position is out of range");
        }
        if (pos == 0)
        {
            T* buf_ptr = new T[size_];
            memcpy(buf_ptr, data_, sizeof(T) * size_);
            data_[0] = val;
            memmove(&data_[1], buf_ptr, sizeof(T) * size_);
            delete[] buf_ptr;
        }
        else if (pos == size_)
        {
            this->push_back(val);
            return;
        }
        else
        {
            T* buf_ptr = new T[size_];
            memcpy(buf_ptr, data_, sizeof(T) * size_);
            data_[pos] = val;
            memmove(&data_[pos + 1], &buf_ptr[pos], sizeof(T) * (size_ - pos));
            delete[] buf_ptr;
        }
        ++size_;
    }    
    void erase(const size_t pos)
    {
        if (pos < 0 || pos >= size_)
        {
            throw std::out_of_range("Specified position is out of range");
        }
        if (pos == 0)
        {
            memmove(data_, &data_[1], sizeof(T) * size_);
        }
        else if (pos == size_)
        {
            this->pop_back();
            return;
        }
        else
        {
            memmove(&data_[pos], &data_[pos + 1], sizeof(T) * (size_ - pos - 1));
        }
        --size_;
    }
    Vector() {
        capacity_ = 0;
        size_ = 0;
        data_ = nullptr;
    };
    ~Vector() {
        delete[] data_;
    };
};