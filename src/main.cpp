#include "custom_vector.hpp"
#include <iostream>
#include <vector>

using namespace std;

template<typename T>
void print_info(Vector<T>& v)
{   
    cout << "----CUSTOM vector----" << endl;
    cout << "capacity: " << v.capacity() << " size: " << v.size() << endl;
    for (auto v_data: v)
    {
        cout << v_data << " ";
    }
    cout << endl;
}

template<typename T>
void print_info(vector<T>& v)
{   
    cout << "----STL vector----" << endl;
    cout << "capacity: " << v.capacity() << " size: " << v.size() << endl;
    for (auto v_data: v)
    {
        cout << v_data << " ";
    }
    cout << endl;
}

template<typename T>
bool test_push_back(Vector<T>& v)
{
    const T val = 42;
    size_t size_before = v.size();
    v.push_back(val);
    size_t size_after = v.size();
    if (size_after - size_before == 1 && *(v.end() - 1) == val)
    {
        cout << "push_back OK\n";
        return true;
    }
    else
    {
        cout << "push_back FAILED\n";
        return false;
    }
}

template<typename T>
bool test_insert(Vector<T>& v)
{
    const T val = 43;
    const size_t pos = 6;
    size_t size_before = v.size();
    v.insert(pos, val);
    size_t size_after = v.size();
    if (size_after - size_before == 1 && v.at(pos) == val)
    {
        cout << "insert OK\n";
        return true;
    }
    else
    {
        print_info(v);
        cout << "insert FAILED\n";
        return false;
    }
}

template<typename T>
bool test_erase(Vector<T>& v)
{
    const int pos = 8;
    size_t size_before = v.size();
    const T element_after_pos = v.at(pos + 1);
    v.erase(pos);
    size_t size_after = v.size();
    if (size_before - size_after == 1 && v.at(pos) == element_after_pos)
    {
        cout << "erase OK\n";
        return true;
    }
    else
    {
        cout << "erase FAILED\n";
        return false;
    }
}

template<typename T>
bool test_pop_back(Vector<T>& v)
{
    const T prelast_elem = *(v.end() - 2);
    size_t size_before = v.size();
    v.pop_back();
    size_t size_after = v.size();
    if (size_before - size_after == 1 && prelast_elem == *(v.end() - 1))
    {
        cout << "pop_back OK\n";
        return true;
    }
    else
    {
        cout << "pop_back FAILED\n";
        return false;
    }
}

void test_custom_vector()
{
    Vector<int> custom_vector;
    vector<int> stl_vector;
    print_info(custom_vector);
    for (int i = 0; i < 17; i++)
    {
        custom_vector.push_back(i + 100);
        stl_vector.push_back(i + 100);
    }
    print_info(custom_vector);
    print_info(stl_vector);
    uint32_t success_counter {0};
    const uint32_t all_test {4}; 
    success_counter += test_push_back(custom_vector);
    success_counter += test_insert(custom_vector);
    success_counter += test_erase(custom_vector);
    success_counter += test_pop_back(custom_vector);

    stl_vector.push_back(42);
    stl_vector.insert(stl_vector.begin() + 6, 43);
    stl_vector.erase(stl_vector.begin() + 8);
    stl_vector.pop_back();

    const int stl_vector_length = (int) stl_vector.size();

    for (int i = 0; i < stl_vector_length; i++)
    {
        if (stl_vector[i] != custom_vector[i])
        {
            cout << "Comparassion FAILED at " << i << endl;
            return;
        }
    }

    cout << "Comparassion completed\n";

    if (all_test == success_counter)
    {
        cout << "PASSED\n";
    }
    else
    {
        cout << "FAILED\n";
    }
}

int main()
{
    test_custom_vector();
}